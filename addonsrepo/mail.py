from email.message import EmailMessage

from aiosmtplib import send
from quart import render_template

from addonsrepo.settings import settings


async def send_email(recipient: str, subject: str, template: str, **context):
    """Send an e-mail"""
    message = EmailMessage()
    message['From'] = settings.mail_from
    message['To'] = recipient
    message['Subject'] = subject
    message.set_content(await render_template(template, base_url=settings.base_url, **context))

    kwargs: dict[str, str | int | bool] = {
        'hostname': settings.mail_hostname,
        'port': settings.mail_port
    }
    if settings.mail_security == 'tls':
        kwargs['use_tls'] = True
        kwargs['start_tls'] = False
    elif settings.mail_security == 'starttls':
        kwargs['use_tls'] = False
        kwargs['start_tls'] = True
    if settings.mail_username:
        kwargs['username'] = settings.mail_username
    if settings.mail_password:
        kwargs['password'] = settings.mail_password

    return await send(message, **kwargs)
