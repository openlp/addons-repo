from quart_sqlalchemy.config import SQLAlchemyConfig
from quart_sqlalchemy.framework import QuartSQLAlchemy

from addonsrepo.settings import settings


_binds: dict[str, dict] = {
    'default': {
        'engine': {
            'url': settings.database_url,
            'echo': settings.database_echo
        },
        'session': {
            'expire_on_commit': settings.database_expire_on_commit
        }
    }
}


if settings.database_url.startswith('sqlite'):
    _binds['default']['engine']['connect_args'] = {
        'check_same_thread': settings.database_check_same_thread
    }


db = QuartSQLAlchemy(config=SQLAlchemyConfig(binds=_binds))
Model = db.Model
Session = db.bind.Session
