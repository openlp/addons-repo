# SPDX-FileCopyrightText: 2024-present Raoul Snyman <raoul@snyman.info>
#
# SPDX-License-Identifier: MIT
from quart_bcrypt import Bcrypt


bcrypt = Bcrypt()
