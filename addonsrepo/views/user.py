from secrets import token_urlsafe

from quart import Blueprint, render_template, request, redirect, url_for, flash, session
from quart_auth import AuthUser, login_user, logout_user
from sqlalchemy.sql.expression import select, or_

from addonsrepo.bcrypt import bcrypt
from addonsrepo.db import Session
from addonsrepo.mail import send_email
from addonsrepo.models import User, UserStatus


user = Blueprint('user', __name__)


@user.route('/login', methods=['GET'])
async def login():
    return await render_template('user/login.html')


@user.route('/login', methods=['POST'])
async def login_post():
    form = await request.form
    with Session() as session:
        user = session.scalars(select(User).where(User.email == form.get('email'),
                                                  User.status == UserStatus.Active)).first()
    if user and bcrypt.check_password_hash(user.password, form.get('password')):
        login_user(AuthUser(user.id), form.get('remember', 'false') == 'true')
        await flash('Logged in successfully', 'success')
        return redirect(url_for('addons.index'))
    else:
        await flash('Email or password are incorrect', 'danger')
        return redirect(url_for('user.login'))


@user.route('/register', methods=['GET'])
async def register():
    username = session.pop('username', None)
    email = session.pop('email', None)
    return await render_template('user/register.html', username=username, email=email)


@user.route('/register', methods=['POST'])
async def register_post():
    form = await request.form
    if not form.get('username') or not form.get('email') or not form.get('password'):
        session['username'] = form.get('username')
        session['email'] = form.get('email')
        await flash('Please fill all fields', 'warning')
        return redirect(url_for('user.register'))
    if form.get('agree-tos', 'false') != 'true':
        session['username'] = form.get('username')
        session['email'] = form.get('email')
        await flash('You need to agree to the terms of service', 'warning')
        return redirect(url_for('user.register'))
    with Session() as sess:
        existing_user = sess.scalars(select(User).where(or_(User.username == form['username'],
                                                            User.email == form['email']))).first()
        if existing_user:
            session['username'] = form.get('username')
            session['email'] = form.get('email')
            await flash('User with these details already exists', 'danger')
            return redirect(url_for('user.register'))
        else:
            user = User(username=form['username'], email=form['email'],
                        password=bcrypt.generate_password_hash(form['password']).decode('utf8'),
                        activation_code=token_urlsafe(48))
            sess.add(user)
            sess.commit()
            sess.refresh(user)
            await send_email(user.email, '[OpenLP Addons] Activate Your Account', 'mail/activation.txt', user=user)
            await flash('Account created! Check your email for an activation link, including your junk folder',
                        'success')
            return redirect(url_for('addons.index'))
    return await render_template('user/register.html')


@user.route('/activate/<code>', methods=['GET'])
async def activate(code: str):
    """Activate a user's account"""
    with Session() as sess:
        user = sess.scalars(select(User).where(User.activation_code == code)).first()
        if not user:
            await flash('Cannot find user with this activation code, your account may already be activated', 'warning')
        else:
            user.status = UserStatus.Active
            user.activation_code = None
            sess.add(user)
            sess.commit()
            await flash('Your account has been activated, please log in', 'success')
    return redirect(url_for('addons.index'))


@user.route('/logout')
async def logout():
    logout_user()
    await flash('You have been logged out', 'success')
    return redirect(url_for('addons.index'))
