# SPDX-FileCopyrightText: 2024-present OpenLP developers
#
# SPDX-License-Identifier: MIT
import re
from functools import wraps
from typing import Callable, Awaitable, TypeVar, ParamSpec

from anyascii import anyascii
from sqlalchemy.sql.expression import select
from quart import current_app, redirect, url_for
from quart.wrappers.request import Request
from quart_auth import current_user

from addonsrepo.db import Session
from addonsrepo.models import Addon, User

T = TypeVar('T')
P = ParamSpec('P')


def title2slug(title: str) -> str:
    """Convert a title to a url slug"""
    return re.sub(r'[^a-z0-9]+', '-', anyascii(title.lower())).strip('-')


async def paginate(request: Request, per_page: int = 9) -> tuple[int, int, int]:
    """Get the "page" parameter from the request, and determine the offset and limit"""
    page = request.args.get('page', 1, type=int)
    limit = request.args.get('perpage', per_page, type=int)
    offset = (page * limit) - limit
    return page, offset, limit


def admin_required(func: Callable[P, Awaitable[T]]) -> Callable[P, Awaitable[T]]:
    """A decorator to restrict route access to admin users"""

    @wraps(func)
    async def wrapper(*args: P.args, **kwargs: P.kwargs) -> T:
        with Session() as session:
            user = session.scalars(select(User).where(User.id == current_user.auth_id)).first()
        if not user.is_admin:
            return redirect(url_for('addons.index'))
        else:
            return await current_app.ensure_async(func)(*args, **kwargs)

    return wrapper


def moderator_required(func: Callable[P, Awaitable[T]]) -> Callable[P, Awaitable[T]]:
    """A decorator to restrict route access to moderators users"""

    @wraps(func)
    async def wrapper(*args: P.args, **kwargs: P.kwargs) -> T:
        with Session() as session:
            user = session.scalars(select(User).where(User.id == current_user.auth_id)).first()
        if not (user.is_admin or user.is_moderator):
            return redirect(url_for('addons.index'))
        else:
            return await current_app.ensure_async(func)(*args, **kwargs)

    return wrapper


async def make_basenames(addon: Addon) -> tuple[str, str]:
    """Create base file names for the thumbnail and the file for the addon"""
    base_dir = str(addon.id)
    file_name = title2slug(addon.title)
    return (
        f'{base_dir}/thumbnail',
        f'{base_dir}/{addon.current_version.version}/{file_name}'
    )
