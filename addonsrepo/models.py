from datetime import datetime
from enum import StrEnum
from uuid import UUID, uuid4

from sqlalchemy.orm import Mapped, mapped_column, relationship, column_property
from sqlalchemy.schema import ForeignKey, Table, Column
from sqlalchemy.sql.expression import select, func
from sqlalchemy.types import Uuid, String, DateTime, Enum, Integer, Text, Boolean

from addonsrepo.db import Model


def _enum2value(enum) -> list[str]:
    return [item.value for item in enum]


addons_tags = Table(
    'addons_tags',
    Model.metadata,
    Column('addon_id', ForeignKey('addons.id')),
    Column('tag_id', ForeignKey('tags.id'))
)


class AddonType(StrEnum):
    """An enumeration containing the allowed addon types"""
    Plugin = 'plugin'
    Theme = 'theme'


class AddonStatus(StrEnum):
    """An enumeration for allowed addons statuses"""
    Unreviewed = 'unreviewed'
    Approved = 'approved'
    Denied = 'denied'
    Disabled = 'disabled'
    Deleted = 'deleted'


class UserStatus(StrEnum):
    """An enumeration containing the allowed user statuses"""
    Unactivated = 'unactivated'
    Active = 'active'
    Disabled = 'disabled'
    Banned = 'banned'


class PageStatus(StrEnum):
    """An enumeration for page statuses"""
    Unpublished = 'unpublished'
    Published = 'published'
    Private = 'private'


class Review(Model):
    """A class to contain a rating or review of an addon"""
    __tablename__ = 'reviews'

    id: Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    user_id: Mapped[int] = mapped_column(Integer, ForeignKey('users.id'), nullable=True)
    addon_id: Mapped[UUID] = mapped_column(Uuid, ForeignKey('addons.id'))
    rating: Mapped[int] = mapped_column(Integer, default=0)
    title: Mapped[str] = mapped_column(String(255))
    description: Mapped[str] = mapped_column(Text, nullable=True)
    created: Mapped[datetime] = mapped_column(DateTime, default=datetime.utcnow)
    updated: Mapped[datetime] = mapped_column(DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    user: Mapped['User'] = relationship(back_populates='reviews')
    addon: Mapped['Addon'] = relationship(back_populates='reviews')


class Tag(Model):
    """A tag for categorising addons"""
    __tablename__ = 'tags'

    id: Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    title: Mapped[str] = mapped_column(String(255))
    description: Mapped[str] = mapped_column(Text, nullable=True)
    created: Mapped[datetime] = mapped_column(DateTime, default=datetime.utcnow)
    updated: Mapped[datetime] = mapped_column(DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)


class Addon(Model):
    """A base class for Addon common properties"""
    __tablename__ = 'addons'

    id: Mapped[UUID] = mapped_column(Uuid, primary_key=True, default=uuid4)
    user_id: Mapped[int] = mapped_column(Integer, ForeignKey('users.id'), nullable=True)
    version_id: Mapped[int] = mapped_column(Integer, ForeignKey('addon_versions.id'), nullable=True)
    title: Mapped[str] = mapped_column(String(255))
    description: Mapped[str] = mapped_column(Text, nullable=True)
    thumbnail_path: Mapped[str] = mapped_column(String(255), nullable=True)
    addon_type: Mapped[AddonType] = mapped_column(Enum(AddonType, values_callable=_enum2value),
                                                  default=AddonType.Theme)
    status: Mapped[AddonStatus] = mapped_column(Enum(AddonStatus, values_callable=_enum2value),
                                                default=AddonStatus.Unreviewed)
    created: Mapped[datetime] = mapped_column(DateTime, default=datetime.utcnow)
    updated: Mapped[datetime] = mapped_column(DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    user: Mapped['User'] = relationship(back_populates='addons', lazy='joined')
    versions: Mapped['AddonVersion'] = relationship(back_populates='addon', foreign_keys='AddonVersion.addon_id')
    current_version: Mapped['AddonVersion'] = relationship(lazy='joined', foreign_keys='Addon.version_id')
    reviews: Mapped[list['Review']] = relationship(back_populates='addon')
    tags: Mapped[list['Tag']] = relationship(secondary=addons_tags)
    theme_properties: Mapped['ThemeProperties'] = relationship(back_populates='addon', lazy='joined')
    plugin_properties: Mapped['PluginProperties'] = relationship(back_populates='addon', lazy='joined')

    rating: Mapped[float] = column_property(
        select(func.avg(Review.rating)).where(Review.addon_id == id).scalar_subquery()
    )


class AddonVersion(Model):
    """A model to track the versions of an addon"""
    __tablename__ = 'addon_versions'

    id: Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    addon_id: Mapped[UUID] = mapped_column(Uuid, ForeignKey('addons.id'), nullable=True)
    version: Mapped[str] = mapped_column(String(255), nullable=True)
    file_path: Mapped[str] = mapped_column(String(255), nullable=True)

    addon: Mapped['Addon'] = relationship(back_populates='versions', foreign_keys='AddonVersion.addon_id')


class PluginProperties(Model):
    __tablename__ = 'plugin_properties'

    id: Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    addon_id: Mapped[UUID] = mapped_column(Uuid, ForeignKey('addons.id'), nullable=True)
    license: Mapped[str] = mapped_column(String(255), nullable=True)

    addon: Mapped['Addon'] = relationship(back_populates='plugin_properties')


class ThemeProperties(Model):
    __tablename__ = 'theme_properties'

    id: Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    addon_id: Mapped[UUID] = mapped_column(Uuid, ForeignKey('addons.id'), nullable=True)
    is_copyright_free: Mapped[bool] = mapped_column(Boolean, default=False)

    addon: Mapped['Addon'] = relationship(back_populates='theme_properties')


class User(Model):
    """A user"""
    __tablename__ = 'users'

    id: Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    email: Mapped[str] = mapped_column(String(255), unique=True)
    username: Mapped[str] = mapped_column(String(255), unique=True)
    password: Mapped[str] = mapped_column(String(255))
    is_admin: Mapped[bool] = mapped_column(Boolean, default=False)
    is_moderator: Mapped[bool] = mapped_column(Boolean, default=False)
    activation_code: Mapped[str] = mapped_column(String(255), nullable=True)
    status: Mapped[UserStatus] = mapped_column(Enum(UserStatus, values_callable=_enum2value),
                                               default=UserStatus.Unactivated)
    created: Mapped[datetime] = mapped_column(DateTime, default=datetime.utcnow)
    updated: Mapped[datetime] = mapped_column(DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    addons: Mapped[list['Addon']] = relationship(back_populates='user')
    reviews: Mapped[list['Review']] = relationship(back_populates='user')

    @property
    def is_active(self):
        """Determine if the user is active or not"""
        return self.status == UserStatus.Active


class Page(Model):
    """A page"""
    __tablename__ = 'pages'

    id: Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    slug: Mapped[str] = mapped_column(String(255), unique=True, index=True)
    title: Mapped[str] = mapped_column(String(255))
    content: Mapped[str] = mapped_column(Text, nullable=True)
    status: Mapped[PageStatus] = mapped_column(Enum(PageStatus, values_callable=_enum2value),
                                               default=PageStatus.Unpublished)
    created: Mapped[datetime] = mapped_column(DateTime, default=datetime.utcnow)
    updated: Mapped[datetime] = mapped_column(DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
