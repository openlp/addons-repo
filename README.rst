OpenLP Addons Repository
========================

This is a web-based repository of addons for OpenLP. At the moment, only Themes and Plugins are supported.


Table of Contents
-----------------

- `Installation <#installation>`_
- `License <#license>`_

Installation
------------

The best way to install and use this is via Docker containers.

Docker CLI
~~~~~~~~~~

.. code-block:: shell

   docker run -v /data/uploads:/uploads -p 8000:8000 registry.gitlab.com/openlp/addons-repo:latest

Docker Compose
~~~~~~~~~~~~~~

.. code-block:: yaml

   services:
     addons-repo:
       image: registry.gitlab.com/openlp/addons-repo:latest
       env:
         - DATABASE_URL=postgresql://user:password@postgres/addons-repo
         - SECRET_KEY=yoursecretkeyhere
       volumes:
         - "./data/addons-repo/uploads:/uploads"
       ports:
         - "127.0.0.1:8000:8000"
       restart: unless-stopped
     postgres:
       image: postgres:15
       env:
         - POSTGRES_USER=user
         - POSTGRES_PASSWORD=password
         - POSTGRES_DB=addons-repo
       volumes:
         - "./data/postgres:/var/lib/postgresql/data"
       restart: unless-stopped


Configuration
-------------

The addons-repo application is configured through environment variables. Here are the options available:

+-----------------------------+-----------------------------------------------------------------+---------------+
| Option                      | Description                                                     | Default value |
+=============================+=================================================================+===============+
| DATABASE_URL                | The URL for the database. This is an `SQLAlchemy database URI`_ | ``sqlite://`` |
+-----------------------------+-----------------------------------------------------------------+---------------+
| DATABASE_ECHO               | Print out database commands. This should be off in production   | False         |
+-----------------------------+-----------------------------------------------------------------+---------------+
| MAIL_HOSTNAME               | Hostname of the mail server used for sending e-mails            | 127.0.0.1     |
+-----------------------------+-----------------------------------------------------------------+---------------+
| MAIL_PORT                   | Port of the mail server used for sending e-mails                | 465           |
+-----------------------------+-----------------------------------------------------------------+---------------+
| MAIL_FROM                   | The e-mail address to send mails from                           |               |
+-----------------------------+-----------------------------------------------------------------+---------------+
| MAIL_USERNAME               | Authenticate with the mail server using this username           |               |
+-----------------------------+-----------------------------------------------------------------+---------------+
| MAIL_PASSWORD               | Authenticate with the mail server using this password           |               |
+-----------------------------+-----------------------------------------------------------------+---------------+
| MAIL_SECURITY               | Security to use when connecting to the mail server. Can either  | ``tls``       |
|                             | blank (no security), ``starttls`` or ``tls``                    |               |
+-----------------------------+-----------------------------------------------------------------+---------------+
| SECRET_KEY                  | This is the key used to encrpyt session cookies and the like.   |               |
|                             | This should never be blank.                                     |               |
+-----------------------------+-----------------------------------------------------------------+---------------+
| UPLOADS_FOLDER              | Where to upload files                                           | ``/uploads``  |
+-----------------------------+-----------------------------------------------------------------+---------------+

Generating a secret key
~~~~~~~~~~~~~~~~~~~~~~~

To generate a secure string for your secret key, you can use the ``secrets`` module in Python, like so:

.. code-block:: shell

   python3 -c 'import secrets; print(secrets.token_urlsafe(48))'


License
-------

``addons-repo`` is distributed under the terms of the `MIT <https://spdx.org/licenses/MIT.html>`_ license.
